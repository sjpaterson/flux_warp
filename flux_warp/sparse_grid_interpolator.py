# ---------------------------------------------------------------------------- #
# 
# Perform radial basis function interpolation on a sparse grid provided a 
# reference array/image is available. 
# Alternatively, perform nearest neighbout interpolation.
# Generalisation of code present in https://github.com/nhurleywalker/fits_warp
#
# ---------------------------------------------------------------------------- #

from __future__ import print_function, division

import numpy as np
import sys
import psutil
import os

from astropy.io import fits
from astropy.wcs import WCS
from scipy.interpolate import Rbf, NearestNDInterpolator, LinearNDInterpolator
from scipy.spatial import KDTree

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def _rbf(arr, x, y, z, interpolation="linear", smooth=0, epsilon=100,
         memfrac=0.75, constrain=True, const=None, absmem="all",
         verbose=True):
    """Sparse grid interpolation with a reference array."""

    lenx, leny = len(x), len(y)

    rbf_ref = np.full_like(np.squeeze(arr), 1. ,dtype=np.float32)

    if interpolation == "nearest": # not using RBF
        z_func = NearestNDInterpolator(np.array([x, y]).T, z)
    elif interpolation == "only_linear":  # true linear interpolation
        z_func = LinearNDInterpolator(np.array([x, y]).T, z)
    else:  # do RBF of some description:

        if constrain:

            n_border = 10

            xy_arr = np.array([x, y]).T
            tree_ = KDTree(xy_arr)
            original_z = z.copy()

            # Here we add some border values to constrain the radial basis
            # function so we do not have boundaries that go to large or small
            # numbers.

            logger.info("determining boundary pixels and values")

            len_arr_x = arr.shape[-2]
            len_arr_y = arr.shape[-1]

            inc_x = len_arr_x // n_border
            inc_y = len_arr_y // n_border

            boundaries = [(0, 0), (len_arr_x, len_arr_y),
                          (len_arr_x, 0), (0, len_arr_y)]

            for i in range(inc_x, len_arr_x-inc_x, inc_x):
                for j in range(inc_y, len_arr_y-inc_y, inc_y):

                    boundaries.append((i, j))

            for boundary in boundaries:

                dist, idx = tree_.query(boundary)

                z_b = original_z[idx]

                x = np.append(x, boundary[0])
                y = np.append(y, boundary[1])
                z = np.append(z, z_b)


        z_func = Rbf(x, y, z, function=interpolation, smooth=smooth, 
                    epsilon=epsilon)


    xy = np.indices(rbf_ref.shape)
    xy.shape = (2, xy.shape[1]*xy.shape[2])

    all_x = xy[0]
    all_y = xy[1]
    all_z = np.full_like(all_x, 1., dtype=np.float32)

    # Determine appropriate memory usage:
    if absmem == "all":
        mem = int(psutil.virtual_memory().available*memfrac)
    else:
        mem = absmem*1024.*1024.
    pixmem = 40000
    stride = mem//pixmem
    stride = (stride//rbf_ref.shape[0])*rbf_ref.shape[0]

    if stride == 0:
        raise MemoryError("Not enough memory available for {:.0f}% memory "
                          " fraction.\n{} MB available\n"
                          "at least {} MB required.".format(memfrac*100, 
                                                            mem/1024./1024.,
                                                            rbf_ref.shape[0] \
                                                            *pixmem/1024./1024.))
        

    if len(all_x) > stride:
        n = 0 

        for i in range(0, len(all_x), stride):

            if verbose:
                    sys.stdout.write(u"\u001b[1000D" + "{:.>6.1f}%".format(100.*i/len(all_x)))
                    sys.stdout.flush()
            n += 1

            rbf_ref[all_x[i:i+stride], all_y[i:i+stride]] = z_func(all_x[i:i+stride].astype(np.float32),
                                                                   all_y[i:i+stride].astype(np.float32))


        print("")
    else:
        all_z = z_func(all_x, all_y)

        rbf_ref[all_x.astype("i"), all_y.astype("i")] = all_z
        # rbf_ref[all_y.astype("i"), all_x.astype("i")] = all_z

    return rbf_ref



def rbf(image, x, y, z, interpolation="linear", smooth=0, epsilon=2, 
        world_coords=False, outname=None, overwrite=True,
        constrain=True, memfrac=0.75, absmem="all",
        return_arr=False):
    """Sparse grid interpolation with a reference FITS image.

    Parameters
    ----------
    image : str
        Reference image.
    x : np.ndarray
        Pixel coordinates along first axis.
    y : np.ndarray
        Pixel coordinates along second axis.
    z : np.ndarray
        Values corresponding to x,y coordinates.
    interpolation : str, optional
        Interpolation method. Passed to `~scpiy.interpolate.Rbf`. Additionally,
        'nearest' or 'linear_only' is available using 
        `~scipy.interpolate.NearestNDInterpolator` and
        `~scipy.interpolate.LinearNDInterpolator`, respectively.
        [Default 'linear']
    smooth : int, optional
        Smoothness parameter passed to `~scipy.interpolate.Rbf`. [Default 0]
    epsilon : float, optional
        Epsilon parameter passed to `~scipy.interpolate.Rbf`. Only used for 
        certain inerpolation functions (e.g. Gaussian). [Default 2]
    world_coords : bool, optional
        True for x,y as world coordinates in decimal degrees. False for x,y as
        pixel coordinates. [Default False]
    outname : str, optional
        Output image name. Default is based on the image name and interpolation
        method. 
    overwrite : bool, optional
        Select ``True`` if wanting to overwrite existing files. [Default True]
    constrain : bool, optional
        Select ``True`` to constrain the RBF methods by using position-weighted
        mean values located around the border of the image. This helps to avoid
        the RBF going to too high or low values at the edges. [Default True]
    memfrac : float, optional
        Fraction of available RAM to use. [Default 0.5]
    absmem : various, optional
        Absolute memory to use. If 'all', then available memory time ``memfrac``
        will be used. [Default 'all']
        
    """


    with fits.open(image) as ref:

        x = np.asarray(x)
        y = np.asarray(y)
        z = np.asarray(z)

        if world_coords:
            w = WCS(ref[0].header)
            if not isinstance(x, np.ndarray):
                x, y = np.asarray(x), np.asarray(y)
            sub_wcs = w.celestial

            y, x = sub_wcs.all_world2pix(x, y, 0)
            x = x.astype("i")
            y = y.astype("i")


        ref[0].data = np.squeeze(ref[0].data)  # we won't use the extra dims
        if len(ref[0].data.shape) > 2:
            ref[0].data = ref[0].data[0, ...]

        rbf_ref = _rbf(ref[0].data, x, y, z, interpolation, smooth, epsilon,
                       constrain=constrain, memfrac=memfrac)

        if not return_arr:

            if outname is None:
                outname = "{}{}.fits".format(image.replace(".fits", ""), interpolation)

            fits.writeto(outname, rbf_ref, ref[0].header, overwrite=overwrite)

    
        else:
            return rbf_ref
