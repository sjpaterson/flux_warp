from __future__ import print_function, division 

import numpy as np
import os

from astropy.io import fits, votable
from astropy.coordinates import SkyCoord
from astropy import units as u

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Conversion between VOTable and FITS dtype:
DTYPE_TO_TFORM = {"f": "E",
                  "i": "I",
                  "S": "A",
                  "U": "A"}


class Catalogue(object):
    """Easy handling of a catalogue."""


    def __init__(self, catalogue, ra_key, dec_key, flux_key=None, 
                 eflux_key=None, localrms_key=None,
                 a_key=None, b_key=None, psf_a_key=None, psf_b_key=None,
                 pa_key=None):
        """When initialised, a number of items will be stored for later.

        Parameters
        ----------
        catalogue : str
            Filepath to a catalogue in a .fits or .vot format.
        ra_key : str
            Key for RA column in ``catalogue``.
        dec_key : str
            Key for DEC column in ``catalogue``.
        flux_key : str, optional
            Key for flux density. Not used if not supplied. [Default None]
        eflux_key : str, optional
            Key for error on flux density. [Default None]
        localrms_key : str, optional
            Key for local rms measurement. [Default None]


        """

        self.name = os.path.basename(catalogue)

        self.table = Catalogue.open_catalogue(catalogue)
        self.ra_key = ra_key
        self.dec_key = dec_key

        self.coords = SkyCoord(ra=self.table[self.ra_key],
                               dec=self.table[self.dec_key],
                               unit=(u.deg, u.deg)) 

        self.flux_key = None
        self.eflux_key = None
        self.localrms_key = None
        self.a_key = None
        self.b_key = None
        self.psf_a_key = None
        self.psf_b_key = None
        self.pa_key = None


        if catalogue.endswith(".fits"):
            self.names = self.table.columns.names
            self.tform = [self.table.columns[name].format 
                          for name in self.table.columns.names]

        elif catalogue.endswith(".vot"):
            self.names = self.table.dtype.names
            self.tform = [DTYPE_TO_TFORM[self.table.dtype[name].kind] 
                          for name in self.names]


        # Add key names for use later on, only if they exist
        if flux_key in self.names:
            self.flux_key = flux_key
        if eflux_key in self.names:
            self.eflux_key = eflux_key
        if localrms_key in self.names:
            self.localrms_key = localrms_key
        if a_key in self.names:
            self.a_key = a_key
        if b_key in self.names:
            self.b_key = b_key
        if psf_a_key in self.names:
            self.psf_a_key = psf_a_key
        if psf_b_key in self.names:
            self.psf_b_key = psf_b_key
        if pa_key in self.names:
            self.pa_key = pa_key


    def exclude(self, exclude_coords, exclusion_zone):
        """Exclude sources within the specified exclusion zone/s."""

        indices = []

        for i in range(len(self.coords)):

            excl_seps = self.coords[i].separation(exclude_coords)
            if (excl_seps.value < exclusion_zone).any():
                continue
            else:
                indices.append(i)

        self.table = self.table[indices]
        self.coords = self.coords[indices]


    def exclude_self(self, exclusion_zone):
        """Internal cross-match to avoid multiply-matched sources."""

        idx, seps, _ = self.coords.match_to_catalog_sky(self.coords, 
                                                        nthneighbor=2)

        indices = np.where(seps.value > exclusion_zone)[0]

        self.table = self.table[indices]
        self.coords = self.coords[indices]


    def only_within(self, coords, radius):
        """Remove sources outside of the radius specified."""

        sep = coords.separation(self.coords)

        self.table = self.table[np.where(sep.value < radius)]
        self.coords = self.coords[np.where(sep.value < radius)]


    def clip(self, threshold):
        """Clip sources based on Jy threshold."""

        if self.flux_key is not None:
            self.coords = self.coords[self.table[self.flux_key] > threshold]
            self.table = self.table[self.table[self.flux_key] > threshold]
        else:
            logger.warning("Thresholding is only possible if the Catalogue "
                           "object was initialised with a flux_key")


    def compact_by_resolution(self, a, b, bmaj, bmin, ratio=1.2):
        """Select only 'compact' sources."""

        if isinstance(bmaj, str):
            cond = np.where(((self.table[a]*self.table[b]) / 
                             (self.table[bmaj]*self.table[bmin])) < ratio)
        else:
            cond = np.where(((self.table[a]*self.table[b]) / 
                             (bmaj*bmin)) <  ratio)

        self.table = self.table[cond]
        self.coords = self.coords[cond]

    def compact_by_flux(self, int_flux, peak_flux, ratio=1.2):
        """Select only 'compact' sources."""

        cond = np.where((self.table[int_flux] / self.table[peak_flux]) < ratio)

        self.table = self.table[cond]
        self.coords = self.coords[cond]


    @staticmethod
    def open_catalogue(catalogue):
        """Open a catalogue file. 

        Must be either .vot or .fits.
        """

        if catalogue.endswith(".vot"):
            table = votable.parse_single_table(catalogue).array
        elif catalogue.endswith(".fits"):
            table = fits.open(catalogue)[1].data
        else:
            raise IOError("Catalogue file ({}) must be either a VOTable (.vot) "
                          "or a FITS table (.fits).".format(catalogue))

        return table


def match(cat1, cat2, separation, exclusion=0.):
    """Match two catalogues.

    Incorporates a minimum separation and an exclusion zone.
    """



    idx1, sep1, _ = cat1.coords.match_to_catalog_sky(cat2.coords)
    idx2, sep2, _ = cat1.coords.match_to_catalog_sky(cat2.coords,
                                                     nthneighbor=2)


    if isinstance(separation, np.ndarray):
        separation = separation[idx1]



    cond = np.where((sep1.value < separation) & (sep2.value > exclusion))[0]
    indices = np.array([cond, idx1[cond]]).T

    if len(idx1[cond]) == 0.:
        raise RuntimeError("No matches between {} and {}".format(cat1.name,
                                                                 cat2.name))
    
    uniq = np.unique(indices[:, 1], return_counts=True)[1]
    indices = indices[np.where(uniq == 1)]

    duplicates = [uniq > 1]
    logger.debug("Removed {} duplicate matches between {} and {}".format(
                  len(duplicates), cat1.name, cat2.name))

    # indices[:, 0] --> cat1 indices
    # indices[:, 1] --> cat2 indices
    return indices 


def check_for_conflicts(cat2):
    """Check for column names that conflict.

    Output names for ``cat1`` will be: 
        * 'old_ra'
        * 'old_dec'
        * 'flux'
        * 'eflux'
        * 'local_rms'

    However we do not need to worry about 'local_rms' since we will ignore this.
    """

    cat1_keys = ["flux", "eflux", "old_ra", "old_dec"]

    for key in cat1_keys:
        if key in cat2.names:
            raise ValueError("{} cannot have the following keys: \n" + \
                             "* flux, eflux, old_ra, old_dec".format(cat2.name))


def write_out(cat1, cat2, indices, outname, nmax=100, 
              ):
    """Write out a catalogue of ``cat2`` coordinates with ``cat1`` appended.

    Parameters
    ----------
    cat1 : Catalogue object
        First/image catalogue.
    cat2 : Catalogue object
        Second/reference catalogue.
    indices : `~np.ndarray`
        2-D array of indices, where 1st dimension is indices in ``cat1`` and 
        second dimension is indices in ``cat2`` that corresponding to the 
        indices in ``cat1``. E.g., output from 
    outname : str
        Name of output FITS table file.
    nmax : int, optional
        Max number of sources in cross-matched catalogue. The actually number may
        be less than this limit. [Default 100]

    Note that Catalogue objects should be prepared earlier with relevant
    flux, eflux, and localrms keys if these columns are wanted.

    """

    check_for_conflicts(cat2)

    if cat1.flux_key is not None and len(indices[:, 0]) > nmax:
        threshold = round(np.sort(cat1.table[cat1.flux_key][indices[:, 0]])[::-1][nmax-1], 1)
        cut = cat1.table[cat1.flux_key][indices[:, 0]] > threshold
    else:
        cut = slice(0, len(indices[:, 0]))

    logger.info("sources in final catalogue: {}".format(
        len(cat1.table[cat1.ra_key][indices[:, 0]][cut])))

    columns_to_add = []

    # 
    for i, column in enumerate(cat2.names):
        if column not in ["flux", "eflux", "local_rms", "major", "minor", "psf_major", "psf_minor"]:  
            # can't have 2 of these!
            columns_to_add += [
                fits.Column(name=column, format=cat2.tform[i],
                            array=cat2.table[column][indices[:, 1]][cut])
                ]


    columns_to_add += [
        fits.Column(name="old_ra", format="E",
                    array=cat1.table[cat1.ra_key][indices[:, 0]][cut]),
        fits.Column(name="old_dec", format="E",
                    array=cat1.table[cat1.dec_key][indices[:, 0]][cut])
        ]

    if cat1.flux_key is not None:
        columns_to_add += [
            fits.Column(name="flux", format="E",
                        array=cat1.table[cat1.flux_key][indices[:, 0]][cut])
            ]

    if cat1.eflux_key is not None:  # do we actually use this for anything?
        columns_to_add += [
            fits.Column(name="eflux", format="E",
                        array=cat1.table[cat1.eflux_key][indices[:, 0]][cut])
            ]

    if cat1.localrms_key is not None:
        columns_to_add += [
            fits.Column(name="local_rms", format="E",
                        array=cat1.table[cat1.localrms_key][indices[:, 0]][cut])
            ]

    if cat1.a_key is not None:
        columns_to_add += [
            fits.Column(name="major", format="E",
                        array=cat1.table[cat1.a_key][indices[:, 0][cut]]/3600.)
            ]

    if cat1.b_key is not None:
        columns_to_add += [
            fits.Column(name="minor", format="E",
                        array=cat1.table[cat1.b_key][indices[:, 0][cut]]/3600.)
            ]

    
    if cat1.pa_key is not None:
        columns_to_add += [
            fits.Column(name="pangle", format="E",
                        array=cat1.table[cat1.pa_key][indices[:, 0][cut]])
            ]


    if cat1.psf_a_key is not None:
        columns_to_add += [
            fits.Column(name="psf_major", format="E",
                        array=cat1.table[cat1.psf_a_key][indices[:, 0][cut]]/3600.)
            ]

    if cat1.psf_b_key is not None:
        columns_to_add += [
            fits.Column(name="psf_minor", format="E",
                        array=cat1.table[cat1.psf_b_key][indices[:, 0][cut]]/3600.)
            ]



    if not outname.endswith(".fits"):
        outname += ".fits"

    hdu = fits.BinTableHDU.from_columns(columns_to_add)
    hdu.writeto(outname, overwrite=True)

