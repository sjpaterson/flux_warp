WARNING: linear and quadratic 2D screens are not currently working!

# flux_warp
Like fits_warp.py but for flux density. Determine if there is a difference in the overall flux scale of an image compared to a model catalogue, then apply a screen of some description to try to bring the image flux density scale in line with the model catalogue. Note that applying a screen is not always applicable even if the flux scale seems to vary wildly, though the code will at the very least give a sense of how the measured flux densities vary even if no corrections are applied. 

Intended for use on Murchsion Widefield Array images (as the model catalogue is suitable for those) but can be applied to any radio interferometry image provided an adequate model catalogue.

Please see the Wiki for details on the methods available for creating the "correction factor map", and see the `--help` text for description of all options. Many options have sensible defaults if using MWA data, using [`aegean`](https://github.com/PaulHancock/Aegean/tree/master/AegeanTools) output catalogues, and if using the built-in `GLEAM_EGC_params.fits` model catalogue.


Tested to work with with Python 2.7 and Python 3.6. LaTeX options for plotting currently only works sometimes due to, I believe, missing fonts on some systems. This code is very much a **work in progress**!

# Installing
After cloning the repository, to install I use the following:
```git clone https://gitlab.com/Sunmish/flux_warp.git
cd flux_warp
python setup.py install --force
```

or if on Pawsey then something like:
```git clone https://gitlab.com/Sunmish/flux_warp.git
cd flux_warp
python setup.py install --force --user
```

or
```git clone https://gitlab.com/Sunmish/flux_warp.git
cd flux_warp
python setup.py install --force --prefix=/path/to/your/python/stuff

```
Note that `--force` is recommended because I am not good at updating the version number.

# Help
A look over the available options when running `flux_warp --help` or `match_catalogues --help` can be useful. Alternatively, looking through the [Wiki](https://gitlab.com/Sunmish/flux_warp/wikis/home) pages might have the answers you are looking for, _though I am in the process of updating these pages_. Otherwise, contacting me directly will(should) also help. 

# Credit
Please include a link to this repository and a citation to [Duchesne et al., 2020](https://ui.adsabs.harvard.edu/abs/2020arXiv200715199D/abstract).
