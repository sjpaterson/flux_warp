#! /bin/bash

# flux_warp and match_catalogues example.
# MWA Phase I data at 88 MHz after infield- and self-calibration. 
# The image has been primary beam corrected, though the fluxscale
# is wonky as heck.

BASENAME="1098637960_MFS-image-pb"
CATALOGUE="${BASENAME}_comp.vot"
IMAGE="${BASENAME}.fits"
MODEL_CATALOGUE="GLEAM_EGC_params.fits"
METHODS=("mean" \
         "median" \
         "linear_screen" \
         "quad_screen" \
         "linear" \
         "nearest" \
         "dec" \
         "elev" \
	     "linear_rbf")
# Set max separation as ~ 1 beamwidth:
separation=$(echo "120/3600" | bc -l)
# Roughly the centre and radius of the image:
coords="60.45 -48.1"
radius=24.8

# Match the .vot image catalogue to the model table:
time match_catalogues \
    ${CATALOGUE} \
    ${MODEL_CATALOGUE} \
    --separation ${separation} \
    --exclusion_zone ${separation} \
    --outname ${BASENAME}_matched.fits \
    --threshold 0.0 \
    --nmax 2000 \
    --coords ${coords} \
    --radius ${radius} \
    --ra1 "ra" \
    --dec1 "dec" \
    --ra2 "ra" \
    --dec2 "dec" \
    --flux_key "int_flux" \
    --eflux_key "err_int_flux" \
    --localrms "local_rms" 

MATCHED_CATALOGUE="${BASENAME}_matched.fits"

for method in ${METHODS[@]}; do

    time flux_warp \
        ${MATCHED_CATALOGUE} \
        ${IMAGE} \
        --mode ${method} \
        --freq 88.0 \
        --outname ${BASENAME}_${method}.fits \
        --threshold 0.0 \
        --nmax 1000 \
        --flux_key "flux" \
        --smooth 5.0 \
        --ignore_magellanic \
        --localrms_key "local_rms" \
        --amp "alpha_c" \
	    --index "beta_c" \
	    --curvature "gamma_c" \
        --ref_flux_key "S154" \
        --ref_freq 154.0 \
        --alpha -0.77 \
        --plot_only \
        --cmap "gnuplot2" \
	    --order 2 \
	    --test_subset \
	    --test_frac 0.5 \

done

time flux_warp \
    ${MATCHED_CATALOGUE} \
    ${IMAGE} \
    --mode mean \
    --freq 88.0 \
    --outname ${BASENAME}_${method}.fits \
    --threshold 0.0 \
    --nmax 1000 \
    --flux_key "flux" \
    --smooth 5.0 \
    --ignore_magellanic \
    --localrms_key "local_rms" \
    --amp "alpha_c" \
    --index "beta_c" \
    --curvature "gamma_c" \
    --ref_flux_key "S154" \
    --ref_freq 154.0 \
    --alpha -0.77 \
    --plot_only \
    --cmap "gnuplot2" \
    --order 2 \
    --test_subset \
    --test_frac 0.5 \
    --update-bscale

exit 0

